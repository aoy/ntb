package main

import (
	"html/template"
	"io/ioutil"
	"os"
)

type Article struct {
	Cat   string
	Title string
	Body  []byte
}

func (a *Article) GetTitle() string {
	return undToSp(a.Title)
}

func loadArticle(cat, title string) (*Article, error) {
	body, err := ioutil.ReadFile(dataDir + cat + "/" + title)
	if err != nil {
		return nil, err
	}
	return &Article{Cat: cat, Title: title, Body: body}, nil
}

func (a *Article) save() error {
	err := ioutil.WriteFile(dataDir+a.Cat+"/"+a.Title, a.Body, 0600)
	if err != nil {
		err = os.Mkdir(dataDir+a.Cat, 0700)
		if err != nil {
			return err
		}
	}
	return ioutil.WriteFile(dataDir+a.Cat+"/"+a.Title, a.Body, 0600)
}

func (a *Article) HTML() template.HTML {
	//	tokens := scan(a.Body)
	b := convert(a.Body)
	return template.HTML(b)
}
