package main

import (
	"github.com/julienschmidt/httprouter"
	"net/http"
)

const (
	dataDir = "./data/"
	port    = ":8070"
)

var menu []Dir

func main() {
	menu = index()
	router := httprouter.New()

	router.GET("/", root)
	router.GET("/index/", viewHome)
	router.GET("/view/:category/:title", valid(viewArticle))
	router.GET("/edit/:category/:title", valid(editArticle))
	router.POST("/save/:category/:title", valid(saveArticle))
	router.GET("/delete/:category/:title", valid(deleteArticle))

	router.ServeFiles("/static/*filepath", http.Dir("./static/"))
	http.ListenAndServe(port, router)
}
