package main

import (
	"log"
	"os"
	"sort"
)

type Dir struct {
	Name  string
	Files []string
}

func index() []Dir {
	dirs, err := content(dataDir)
	if err != nil {
		log.Fatal(err)
	}
	var res = make([]Dir, len(dirs))
	var j int
	for i := 0; i < len(dirs); i++ {
		files, err := content(dataDir + "/" + dirs[i])
		if err != nil {
			log.Println(dirs[i], err)
			continue
		}
		res[j] = Dir{dirs[i], files}
		j++
	}
	return res[:j]
}

func content(dir string) ([]string, error) {
	d, err := os.Open(dir)
	if err != nil {
		return nil, err
	}
	defer d.Close()
	s, err := d.Readdirnames(-1)
	if err != nil {
		return nil, err
	}
	sort.Strings(s)
	return s, nil
}
