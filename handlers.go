package main

import (
	"html/template"
	"log"
	"net/http"
	"os"
	"regexp"

	"github.com/julienschmidt/httprouter"
)

type Page struct {
	Menu []Dir
	*Article
}

var validator = regexp.MustCompile(`^[\pL0-9_]+$`)
var funcMap = template.FuncMap{"title": undToSp}
var tmpl = template.Must(template.New("").Funcs(funcMap).ParseGlob("templates/*.html"))

func valid(fn func(http.ResponseWriter, *http.Request, string, string)) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		cat, title := ps.ByName("category"), ps.ByName("title")
		cat = spToUnd(cat)
		title = spToUnd(title)
		if !validator.MatchString(cat) || !validator.MatchString(title) {
			println("valid2",cat,title)
			http.Error(w, "Bad Request", http.StatusBadRequest)
			return
		}
		fn(w, r, cat, title)
	}
}

func root(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	http.Redirect(w, r, "/index", http.StatusFound)
}

func viewHome(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	err := tmpl.ExecuteTemplate(w, "index.html", menu)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func undToSp(s string) string {
	var res = []byte(s)
	for i := range res {
		if res[i] == '_' {
			res[i] = ' '
		}
	}
	return string(res)
}

func spToUnd(s string) string {
	var res = []byte(s)
	for i := range res {
		if res[i] == ' ' {
			res[i] = '_'
		}
	}
	return string(res)
}

func rm(d, f string) {
	err := os.Remove(dataDir + d + "/" + f)
	if err != nil {
		log.Println(err)
		return
	}
	err = os.Remove(dataDir + d) //Remove dir if empty
	if err != nil {
		log.Println(err)
	}
}

func viewArticle(w http.ResponseWriter, r *http.Request, cat, title string) {
	art, err := loadArticle(cat, title)
	if err != nil {
		http.Redirect(w, r, "/index", http.StatusFound)
		return
	}
	err = tmpl.ExecuteTemplate(w, "view.html", &Page{menu, art})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func editArticle(w http.ResponseWriter, r *http.Request, cat, title string) {
	art, err := loadArticle(cat, title)
	if err != nil {
		art = &Article{Cat: cat, Title: title}
	}
	err = tmpl.ExecuteTemplate(w, "edit.html", &Page{menu, art})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func saveArticle(w http.ResponseWriter, r *http.Request, cat, title string) {
	c := spToUnd(r.FormValue("cat"))
	t := spToUnd(r.FormValue("title"))
	if !validator.MatchString(c) || !validator.MatchString(t) {
		http.Error(w, "Bad Request", http.StatusBadRequest)
		return
	}
	art := &Article{Cat: c, Title: t, Body: []byte(r.FormValue("body"))}
	err := art.save()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
	//Remove old article if the category and/or the title were changed
	if cat != c || title != t {
		rm(cat, title)
	}
	menu = index()
	http.Redirect(w, r, "/view/"+art.Cat+"/"+art.Title, http.StatusFound)
}

func deleteArticle(w http.ResponseWriter, r *http.Request, cat, title string) {
	println(cat, title)
	rm(cat, title)
	menu = index()
	http.Redirect(w, r, "/index", http.StatusFound)
}
