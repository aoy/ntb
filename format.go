package main

import (
	"bytes"
)

const (
	text = iota
	h2
	code
	bold
)

type Token struct {
	typ   int
	start int
	end   int
}

type tag struct {
	typ   int
	start []byte
	end   []byte
}

var tags1 = []tag{
	{h2, []byte("# "), []byte("\n")},
	{code, []byte("``"), []byte("``")},
	{bold, []byte("**"), []byte("**")},
}

var tags2 = []tag{
	{h2, []byte(`<div id="h2">`), []byte(`</div>`)},
	{code, []byte(`<div id="code">`), []byte(`</div>`)},
	{bold, []byte(`<b>`), []byte(`</b>`)},
}

func addTag(b, buf []byte, pos, n int) ([]byte, int) {
	var newPos int
	var x = len(tags1[n].start)
	m := bytes.Index(b[pos+x:], tags1[n].end)
	if m < 0 {
		buf = append(buf, b[pos:len(b)]...)
		newPos = len(b)
		return buf, newPos
	}
	m += pos + x
	buf = append(buf, tags2[n].start...)
	buf = append(buf, b[pos+len(tags1[n].start):m]...)
	buf = append(buf, tags2[n].end...)
	newPos = m + len(tags1[n].end)
	return buf, newPos
}

func convert(b []byte) []byte {
	buf := make([]byte, 0, 2*len(b))
	var k int
	for i := 0; i < len(b)-1; i++ {
		for j := range tags1 {
			if !bytes.HasPrefix(b[i:], tags1[j].start) {
				continue
			}
			buf = append(buf, b[k:i]...)
			buf, k = addTag(b, buf, i, j)
			i = k - 1
		}
	}
	if k < len(b) {
		buf = append(buf, b[k:len(b)]...)
	}
	return buf
}
